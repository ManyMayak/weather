package com.example.erik.weatherapp.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoCity {

    @SerializedName("f")
    @Expose
    private boolean f;
    @SerializedName("n")
    @Expose
    private boolean n;
    @SerializedName("nr")
    @Expose
    private boolean nr;
    @SerializedName("ns")
    @Expose
    private boolean ns;
    @SerializedName("nsr")
    @Expose
    private boolean nsr;
    @SerializedName("p")
    @Expose
    private boolean p;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("lon")
    @Expose
    private double lon;
    @SerializedName("tzinfo")
    @Expose
    private Timezone tzinfo;
    @SerializedName("def_pressure_mm")
    @Expose
    private int defPressureMm;
    @SerializedName("def_pressure_pa")
    @Expose
    private int defPressurePa;
    @SerializedName("_h")
    @Expose
    private boolean h;
    @SerializedName("url")
    @Expose
    private String url;

    public boolean isF() {
        return f;
    }

    public void setF(boolean f) {
        this.f = f;
    }

    public boolean isN() {
        return n;
    }

    public void setN(boolean n) {
        this.n = n;
    }

    public boolean isNr() {
        return nr;
    }

    public void setNr(boolean nr) {
        this.nr = nr;
    }

    public boolean isNs() {
        return ns;
    }

    public void setNs(boolean ns) {
        this.ns = ns;
    }

    public boolean isNsr() {
        return nsr;
    }

    public void setNsr(boolean nsr) {
        this.nsr = nsr;
    }

    public boolean isP() {
        return p;
    }

    public void setP(boolean p) {
        this.p = p;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Timezone getTzinfo() {
        return tzinfo;
    }

    public void setTzinfo(Timezone tzinfo) {
        this.tzinfo = tzinfo;
    }

    public int getDefPressureMm() {
        return defPressureMm;
    }

    public void setDefPressureMm(int defPressureMm) {
        this.defPressureMm = defPressureMm;
    }

    public int getDefPressurePa() {
        return defPressurePa;
    }

    public void setDefPressurePa(int defPressurePa) {
        this.defPressurePa = defPressurePa;
    }

    public boolean isH() {
        return h;
    }

    public void setH(boolean h) {
        this.h = h;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
