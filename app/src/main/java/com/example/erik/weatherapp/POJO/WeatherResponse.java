package com.example.erik.weatherapp.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherResponse {

    @SerializedName("now")
    @Expose
    private int now;
    @SerializedName("now_dt")
    @Expose
    private String nowDt;
    @SerializedName("info")
    @Expose
    private InfoCity info;
    @SerializedName("fact")
    @Expose
    private FactWeather fact;
    @SerializedName("forecasts")
    @Expose
    private List<Forecast> forecasts = null;

    public int getNow() {
        return now;
    }

    public void setNow(int now) {
        this.now = now;
    }

    public String getNowDt() {
        return nowDt;
    }

    public void setNowDt(String nowDt) {
        this.nowDt = nowDt;
    }

    public InfoCity getInfo() {
        return info;
    }

    public void setInfo(InfoCity info) {
        this.info = info;
    }

    public FactWeather getFact() {
        return fact;
    }

    public void setFact(FactWeather fact) {
        this.fact = fact;
    }

    public List<Forecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(List<Forecast> forecasts) {
        this.forecasts = forecasts;
    }

}
