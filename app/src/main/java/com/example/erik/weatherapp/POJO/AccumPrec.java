package com.example.erik.weatherapp.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccumPrec {

    @SerializedName("1")
    @Expose
    private int _1;
    @SerializedName("3")
    @Expose
    private double _3;
    @SerializedName("7")
    @Expose
    private double _7;

    public int get1() {
        return _1;
    }

    public void set1(int _1) {
        this._1 = _1;
    }

    public double get3() {
        return _3;
    }

    public void set3(double _3) {
        this._3 = _3;
    }

    public double get7() {
        return _7;
    }

    public void set7(double _7) {
        this._7 = _7;
    }

}
